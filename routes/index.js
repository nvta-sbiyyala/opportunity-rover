const express = require('express');
const router = express.Router();
const db = require('../database/db');

/* GET root */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/insert', (req, res, next) => {
  db.insert();
  return res.sendStatus(200)
});

router.post('/update', (req, res, next) => {
  db.update();
  return res.sendStatus(200)
});

router.post('/delete', (req, res, next) => {
  db.delete();
  return res.sendStatus(200)
});


module.exports = router;
