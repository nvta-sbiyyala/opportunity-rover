FROM node:alpine3.10

WORKDIR /opportunity

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm", "run", "debug" ]
