# opportunity-rover

A PoC Project for Postgres, Debezium, and Kafka

## Overview

Opportunity publishes changes to its PostgreSQL database via the debezium-connector to Kafka where it can be consumed by [Curiosity](https://gitlab.com/jstone28/curiosity-rover)

![overview](docs/overview.png)

## Getting Started

To run the entire, end to end use case:

```bash
./startup
```

That should kick off an environment setup, debezium configuration, and leave an open shell open within kafka so messages can be seen. From there, `POST` requests scan be made against Opportunity's (`localhost:3000/insert`).

```bash
curl --location --request GET 'localhost:3000/insert'
```

`POST`ing to the `insert/` endpoint will prompt an insert command within Opportunity.

```javascript
'INSERT INTO opportunity(username) VALUES($1) RETURNING *'
```

To see messages come through, `POST` to `localhost:3000/insert` and watch the open console. Additionally, you can follow the logs of [Curiosity](https://gitlab.com/jstone28/curiosity-rover) to see messages gathered from the kafka declared by Opportunity (`postgres.public.opportunity`)

```bash
docker-compose logs -f curiosity
```
